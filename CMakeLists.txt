cmake_minimum_required(VERSION 3.8)
project(StatisticMultiset)

set(CMAKE_CXX_STANDARD 17)

set(SOURCE_FILES main.cpp StatisticMultiset.h StatisticMultiset.cpp)
add_executable(StatisticMultiset ${SOURCE_FILES})