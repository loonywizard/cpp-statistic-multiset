//
// Created by vladimir on 25.09.17.
//

#include "StatisticMultiset.h"
#include <fstream>

StatisticMultiset::StatisticMultiset() {}

StatisticMultiset::StatisticMultiset(const char* filename) {
  this->addNumbersFromFile(filename);
}

void StatisticMultiset::addNumbersFromFile(const char *filename) {
  std::ifstream fin(filename);
  int tmp;
  while (fin >> tmp) {
    this->numbers.push_back(tmp);
  }
  fin.close();
}

void StatisticMultiset::insert(StatisticMultiset *statisticMultiset) {
  for (int i = 0; i < statisticMultiset->numbers.size(); i++) {
    this->numbers.push_back(statisticMultiset->numbers[i]);
  }
}

void StatisticMultiset::insert(int n) {
  this->numbers.push_back(n);
}

int StatisticMultiset::getMax() const {
  int max = this->numbers[0];
  for (int i = 1; i < this->numbers.size(); i++) {
    if (this->numbers[i] > max) {
      max = this->numbers[i];
    }
  }
  return max;
}

int StatisticMultiset::getMin() const {
  int min = this->numbers[0];
  for (int i = 1; i < this->numbers.size(); i++) {
    if (this->numbers[i] < min) {
      min = this->numbers[i];
    }
  }
  return min;
}

float StatisticMultiset::getAverage() const {
  long long sum = 0;
  for (int i = 0; i < this->numbers.size(); i++) {
    sum += this->numbers[i];
  }
  return (float)sum / this->numbers.size();
}

int StatisticMultiset::getCountAbove(int n) const {
  int count = 0;
  for (int i = 0; i < this->numbers.size(); i++) {
    if (this->numbers[i] > n) {
      count++;
    }
  }
  return count;
}

int StatisticMultiset::getCountUnder(int n) const {
  int count = 0;
  for (int i = 0; i < this->numbers.size(); i++) {
    if (this->numbers[i] < n) {
      count++;
    }
  }
  return count;
}