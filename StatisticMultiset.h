//
// Created by vladimir on 21.09.17.
//

#ifndef STATISTICMULTISET_STATISTICMULTISET_H
#define STATISTICMULTISET_STATISTICMULTISET_H

#include <vector>

class StatisticMultiset {
public:
  StatisticMultiset();
  StatisticMultiset(const char* filename);
  std::vector<int> numbers;

  void insert(int n);
  void insert(StatisticMultiset* statisticMultiset);
  void addNumbersFromFile(const char* filename);
  int getMax() const;
  int getMin() const;
  float getAverage() const;
  int getCountUnder(int n) const;
  int getCountAbove(int n) const;
};

#endif //STATISTICMULTISET_STATISTICMULTISET_H
