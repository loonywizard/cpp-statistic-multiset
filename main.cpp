#include <iostream>
#include "StatisticMultiset.h"

int main() {
  StatisticMultiset statisticMultiset1("../data.statistic-multiset");
  StatisticMultiset statisticMultiset;
  statisticMultiset.insert(&statisticMultiset1);

  statisticMultiset.insert(1);
  statisticMultiset.insert(2);
  statisticMultiset.insert(3);
  statisticMultiset.insert(4);
  statisticMultiset.insert(0);

  std::cout << statisticMultiset.getMin() << std::endl;
  std::cout << statisticMultiset.getMax() << std::endl;
  std::cout << statisticMultiset.getAverage() << std::endl;
  std::cout << statisticMultiset.getCountUnder(2) << std::endl;
  std::cout << statisticMultiset.getCountAbove(2) << std::endl;
  return 0;
}